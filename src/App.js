import logo from './logo.svg';
import './App.css';

import React, { Component } from 'react'
import Users from './components/Users';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      posts: null,
      comments: null,
      status: false
    }
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users").then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        users: data,

      })
      return fetch("https://jsonplaceholder.typicode.com/posts");
    }).then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        posts: data,

      })
      return fetch("https://jsonplaceholder.typicode.com/comments");
    }).then((res) => {
      return res.json();
    }).then((data) => {

      this.setState({
        comments: data,
        status: true

      })

    })
  }


  render() {
    return (
      <div className='App'>
        {
          (this.state.status) ?
            <div>
              <Users users={this.state.users} posts={this.state.posts} comments={this.state.comments} />
            </div>
            : null
        }
      </div>
    )
  }
}



export default App;

