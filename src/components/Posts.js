import React, { Component } from 'react';
import Comments from './Comments';
import './post.css';
import Optionicon from '../images/option.png';

export default class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: null,
            received: false
        }
    }



    show = (id) => {

        const listpost = this.props.posts.filter((Element) => {
            return Element.userId == id;
        })
        this.setState({
            list: listpost,
            received: !this.state.received
        })


    }
    render() {

        return (
            <div class="post-container">
                <div>
                    <div className='post-heading'>
                        Posts

                    </div>
                    <div> <button onClick={() => { this.show(this.props.userId) }}><img src={Optionicon} className='option-icon' /></button></div>

                </div>
                {
                    (this.state.received) ?
                        <div className='post-content'>
                            {this.state.list.map((element) => {

                                return (
                                    <div key={element.id} class="inner-content">
                                        <div class="post-text">
                                            <div className='post-name'><h3>{element.title}</h3></div>
                                            <div className='post-body'>{element.body}</div>
                                        </div>
                                        <div> <Comments comments={this.props.comments} postId={element.id} /></div>
                                    </div>
                                )


                            })}
                        </div> : null

                }




            </div>
        )
    }
}
