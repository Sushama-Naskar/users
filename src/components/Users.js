import React, { Component } from 'react'
import Posts from './Posts';
import './user.css';
import Usericon from '../images/user.png';
import Placeholdericon from '../images/placeholder.png';
import Suitcaseicon from '../images/suitcase.png';
import Phoneicon from '../images/phone.png';
import Emailicon from '../images/email.png';
import Worldicon from '../images/world-wide-web.png';
import Interneticon from '../images/internet.png';

export default class Users extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.users.map((user) => {
                    return (<div key={user.id}>
                        <div className='user'>
                            <div ><img src={Usericon} className="user-icon" /> </div>
                            <div><h2>{user.name}({user.username})</h2></div>
                            <div><img src={Placeholdericon} className='placeholder-icon' />{user.address.street},{user.address.city}</div>
                            <div><img src={Suitcaseicon} className='suitcase-icon' />{user.company.name}</div>
                            <div className="contact">
                                <div className='item item-1'><div><img src={Phoneicon} className='phone-icon' /></div>{user.phone}</div>
                                <div className='item item-2'><div><img src={Emailicon} className='email-icon' /></div>{user.email}</div>
                                <div className='item item-3'><div><img src={Interneticon} className='internet-icon' /></div>{user.website}</div>
                            </div>

                            <Posts posts={this.props.posts} comments={this.props.comments} userId={user.id} />
                        </div>
                    </div>
                    )

                })}

            </div>
        )
    }
}
