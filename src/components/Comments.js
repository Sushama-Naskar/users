import React, { Component } from 'react';
import './comments.css';
import Chaticon from '../images/chat-bubble.png';
import User1 from '../images/user (1).png';

export default class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: null,
            received: false
        }
    }
    show = (id) => {

        const commentpost = this.props.comments.filter((Element) => {
            return Element.postId == id;
        })
        this.setState({
            list: commentpost,
            received: !this.state.received
        })


    }
    render() {
        return (
            <div className="comment-container">
                <div className="heading">
                    <div className='comment-heading'>
                        Comments
                    </div>
                    <button onClick={() => { this.show(this.props.postId) }}><img src={Chaticon} className='chat-icon' /></button>
                </div>
                {
                    (this.state.received) ?
                        <div className='comment-content'>
                            {this.state.list.map((element) => {

                                return (
                                    <div key={element.id}>
                                        <div className='comment-text'>
                                            <div class="user-text"><img src={User1} className='user1-icon' />{element.name} ({element.email})</div>
                                            <div class="comment-body">{element.body}</div>
                                        </div>

                                    </div>
                                )


                            })}
                        </div> : null

                }




            </div>
        )
    }
}
